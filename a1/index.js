// activity s16

//  part 1

    let num1 = 25;
	let num2 = 5;
	console.log("The result of num1 + num2 should be 30.");
	console.log("Actual Result:");
	console.log(num1 + num2);

	let num3 = 156;
	let num4 = 44;
	console.log("The result of num3 + num4 should be 200.");
	console.log("Actual Result:");
	console.log(num3 + num4);

	let num5 = 17;
	let num6 = 10;
	console.log("The result of num5 - num6 should be 7.");
	console.log("Actual Result:");
	console.log(num5-num6);
// 
 

// part 2 

	let minutesHour = 60;
	let hoursDay = 24;
	let daysWeek = 7;
	let weeksMonth = 4;
	let monthsYear = 12;
	let daysYear = 365

 let minInYear = minutesHour * hoursDay * daysYear;
	console.log('there are ' + minInYear + ' minutes in a year.');	

// part 3 

   let tempCelsius = 132;
   let celsius = 1.8;
   let need = 32;	

  let celToFah = tempCelsius * celsius + need;
	console.log(tempCelsius + ' degrees celsius when converted to fahrenheit is ' + celToFah);
// 

// part 4 
	let num7 = 165;
	let num8 = 8;

	let remainder = num7 % num8;
	console.log('The remainder of 165 divided by 8 is: ' + remainder);
	console.log("Is num7 divisible by 8?"); 

	console.log(num7 == num8);

// 

// part 5 
 
   let num88 = 348;
   let num9 = 4;

   let part5 = num88 % num9;
	console.log('the remainder of 348 divided by 4 is: ' + part5);
	console.log("Is num8 divisible by 4?");

	console.log(num88 !== num9);

