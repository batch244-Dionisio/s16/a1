console.log("Hellow World?");

// [section] arithmetic operators
	
	let	numA = 200;
	let	numB = 6;
	
	console.log(2);

	// sum of a numA and numB;
	let sum = numA + numB;
	console.log(sum);

	// different of numA & numB;
	let difference = numA - numB;
	console.log(difference);

	// get the product of numA & numB;
	let product = numA * numB;
	console.log(product);

	// qoutient of numA & numB;
	let qoutient = numA/ numB;
	console.log(qoutient);

	// remainder/modulo
	let remainder = numA % numB;
	console.log(remainder);
// 

// [section] assignment operator

	// basic assignment operator (=) 

	let assignedNumber = 8;
	console.log(assignedNumber);

	assignedNumber = assignedNumber + 2;
	console.log('Result of addition assignment operator: ' + assignedNumber)

	assignedNumber += 1;
	console.log('Result of addition assignment operator: ' + assignedNumber)

	// /mdas (-=, *= /=)

	assignedNumber  -= 2;
	console.log('Result	of subtraction assignment operator: ' +assignedNumber);

	assignedNumber  *= 2;
	console.log('Result	of multiplication assignment operator: ' +assignedNumber);

	assignedNumber  /= 2;
	console.log('Result	of division assignment operator: ' +assignedNumber);
// 

// [section] multiple operators and parentheses

	let mdas = 1 + 2 -3 * 4 / 5;
	console.log('Result of mdas oepration: ' + mdas);

	/**/

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log('Result of pemdas oepration:' + pemdas);

	/*
		1. 4 / 5 = 0.8
		2. 2 - 3 = -1
		3. -1 * 0.8 = -0.8
		4. 1 + -0.8 = 0.2
	*/
// 

// [section] increment and decrement

	 //pre-increment
	 let blankets = 10; // may continuity
	 let newBlankets = ++blankets;
	 console.log("newBlankets:" + newBlankets); 

	 // pos-increment 
	 let pillows = 10; // walang continuity
	 let newPillows = pillows++;
	 console.log('pillows: ' + pillows);
	 console.log("newPillows: " + newPillows);

	 // pre- decrement
	 let soda = 10;
	 let newSoda = --soda;
	 console.log('soda: ' + soda);
	 console.log('newSoda:' + newSoda);

	 // post-decrement

	 let pasta = 10;
	 let newPasta = pasta--;
	 console.log('pasta: ' + pasta);
	 console.log('newPasta' + newPasta);
//

// [section] comparison operators

	let juan = 'juan';

		 // equality operator (==) check if theyre the same

		 // returns a boolean value
		 console.log(1 == 1); // true
		 console.log(1 == 2); // false

		 console.log(1 == '1'); // true
		 console.log (0 == false); // true

		console.log ('juan' == "juan"); // true
		console.log('juan' == juan ); // true

		// inequality operator (!=) check if they're not the same

		console.log(1 != 1); // false
		console.log(2 != 1); // true

		console.log(1 != '1'); // fasle
		console.log(0 != false); // false
		console.log('juan' != 'juan') // false

		console.log('juan' != juan); // false	

		// strict equality operator (===) ensure that data types provided correct

		console.log (1 === 1);  // true
		console.log (2 === 1);  // false
		console.log (1 === "1");  // fasle
		console.log (0 === false);  // false
		console.log( 'juan' === 'juan'); // true
		console.log(juan === 'juan');  // true (because let juan = 'juan'; )

		// strict inequality operator (!==) 
		console.log(1 !== 1);  // false
		console.log(1 !== 2);  // true

		console.log(1 !== '1'); // true
		console.log(0 !== false);  // true
		console.log('juan' !== 'juan'); // false
		console.log('juan' !== juan);  // false
// 

// [section] relational operators 
	// grarter or less than value

	let a = 50;
	let b = 65;
	let c = 1;

	   // greater than operator ( > )

	   let isGreaterThan = a > b;
	   console.log(isGreaterThan);  // false

	   // less than operator ( < )
		let	isLessThan = a < b;
		console.log(isLessThan);  // true

		// greater than or equal operator ( >= )

		 let isGtorEqual = a >= b;
		 console.log(isGtorEqual);  // false

	   // less than or equal operator ( <= )
	   	let isLtorEqual = a <= b;
	   	console.log(isLtorEqual);

	   		let numString = '30';
	   		console.log(a > numString);  // true
	   		console.log(b <= numString);   // false

	   		let string = 'twenty'
	   		console.log( b >= string);
// 

// [section ] logical operators 
 	
 	let isLegalAge = true;
 	let isRegistered = false;

 		// logical and operator ( && - double ampersand)
 			// all true 

 			let allRequirementsMet = isLegalAge && isRegistered;
 			console.log('Result of logical AND operator: ' + allRequirementsMet);

 		// logical OR operator ( || - double pipe )
			 // true if ONE of the operands is true

			let someRequirementsMet = isLegalAge || isRegistered;
			console.log('Result of logical OR operator: ' + someRequirementsMet); 

		// logical NOT operator (! - exclamation point)
		    // opposite value
			
			let someRequirementsNotMet = !isRegistered;
			console.log('Result of logical NOT 	operator: ' + someRequirementsNotMet);